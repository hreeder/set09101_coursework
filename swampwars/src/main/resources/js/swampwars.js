$(document).ready(function() {
    $('#changeDiet').click(function () {
        $('#changeDiet').off('click');
        $('#currentDiet').html('<select id="newDiet" class="form-control">' +
                '<option value="null">Please select a diet</option>' +
                '<option value="knights">Tennents</option>' +
                '<option value="enemies">Coffee</option>' +
                '<option value="bigmacs">Mince Pies</option>' +
            '</select>');

        $('#newDiet').change(function () {
            console.log($('#newDiet option:selected').val());
            $('#changeDiet').attr('href', '/diet/' + $('#newDiet option:selected').val());
        });
    });
});