package uk.co.harryreeder.set09101.swampwars.swamp.enemies;

import uk.co.harryreeder.set09101.swampwars.GameState;

/**
 * Created by harry on 27/11/2015.
 */
public class Donkey extends OgreEnemy {
    public Donkey(String name) {
        super(name);
    }

    public Donkey(GameState state) {
        super("Hons", state);
    }

    public Donkey(String name, GameState state) {
        super(name, state);
    }
}
