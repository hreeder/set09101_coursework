package uk.co.harryreeder.set09101.swampwars;

import spark.ModelAndView;
import spark.template.jade.JadeTemplateEngine;
import uk.co.harryreeder.set09101.swampwars.swamp.Actor;
import uk.co.harryreeder.set09101.swampwars.swamp.Ogre;
import uk.co.harryreeder.set09101.swampwars.swamp.diet.BigMacDiet;
import uk.co.harryreeder.set09101.swampwars.swamp.diet.KnightsInShiningArmorDiet;
import uk.co.harryreeder.set09101.swampwars.swamp.diet.OgreEnemyDiet;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import static spark.Spark.*;

/**
 * DONKEY
 * GET
 * OUT
 * MY
 * SWAMP
 *
 */
public class App 
{
    private static final Logger logger = Logger.getLogger(App.class.getName());
    public static void main( String[] args )
    {
    	// App Setup
    	int serverPort = 9090;
    	port(serverPort);
        staticFileLocation("/public");
    	JadeTemplateEngine jade = new JadeTemplateEngine();

        GameState game = new GameState();

        // This is where we define the web routes
    	get("/", (req, res) -> {
              // ModelAndView takes two args - our "model" - ie the HashMap
              // And the view name, where it will search src/main/resources/templates
              // for the given name .jade - in this case: swamp.jade
              ModelAndView view = new ModelAndView(game.getState(), "swamp");
              return view;
        }, jade);

        post("/game/start", (req, res) -> {
            int rows = Integer.parseInt(req.queryParams("rows"));
            int cols = Integer.parseInt(req.queryParams("cols"));

            if (!game.isStarted()) {
                game.startGame(rows, cols);
            }

            res.redirect("/");
            return true;
        });

        /*
            ACTIONS
         */
        get("/move", (req, res) -> {
            ArrayList<Thread> threads = new ArrayList<Thread>();
            MoveCommand cmd = new MoveCommand(game.getOgre(), game);
            threads.add(new Thread(cmd));

            for (Actor enemy : game.getEnemies()) {
                MoveCommand moveEnemy = new MoveCommand(enemy, game);
                threads.add(new Thread(moveEnemy));
            }

            threads.forEach(Thread::start);

            boolean allFinished = false;
            while(!allFinished) {
                int numFinished = 0;
                for (Thread thread : threads) {
                    if (thread.isAlive() == false) {
                        numFinished++;
                    }
                }
                if (numFinished == threads.size()) {
                    allFinished = true;
                }
            }

            game.resolvePotentialConflicts();

            // We have a 1 in 3 chance of spawning a new enemy
            // Create a random int and if it's 1, create an enemy
            int chance = ThreadLocalRandom.current().nextInt(1,4);
            if (chance == 1) {
                logger.info("Creating Enemy");
                game.createEnemy();
            }

            res.redirect("/");
            return true;
        });

        get("/diet/:newDiet", (req, res) -> {
            String newDietString = req.params(":newDiet");
            Ogre ogre = game.getOgre();
            switch (newDietString) {
                case "enemies":
                    game.getOgre().setDiet(new OgreEnemyDiet());
                    break;
                case "knights":
                    game.getOgre().setDiet(new KnightsInShiningArmorDiet());
                    break;
                case "bigmacs":
                    game.getOgre().setDiet(new BigMacDiet());
                    break;
                default:
                    break;
            }
            game.gameLog.add(new StringBuilder()
                    .append(game.getOgre().getName())
                    .append(" has changed their diet to ")
                    .append(game.getOgre().getDiet().getDietName())
                    .append(".")
                    .toString());
            res.redirect("/");
            return true;
        });
        get("/reset", (req, res) -> {
            game.reset();
            res.redirect("/");
            return true;
        });
  	
    	System.out.println("Please navigate to http://127.0.0.1:" + Integer.toString(serverPort) + "/ in your web browser");
    }
}
