package uk.co.harryreeder.set09101.swampwars.swamp.diet;

/**
 * Created by harry on 26/11/2015.
 */
public interface OgreDiet {
    String DIET_NAME = null;

    /**
     * Returns true if the ogre will eat the corresponding number of enemies
     * @param numEnemies Number of enemies the ogre will try to eat
     * @return True if eat successful. False if eat unsuccessful
     */
    boolean eat(int numEnemies);

    String getDietName();
}
