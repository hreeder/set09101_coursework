package uk.co.harryreeder.set09101.swampwars.swamp.enemies;

import uk.co.harryreeder.set09101.swampwars.GameState;
import uk.co.harryreeder.set09101.swampwars.swamp.Actor;

public class OgreEnemy extends Actor {

    public OgreEnemy(String name) {
        super(name);
    }

    public OgreEnemy(GameState state) {
        this("ENEMY", state);
    }

    public OgreEnemy(String name, GameState state) {
        super(name, state);
    }
}
