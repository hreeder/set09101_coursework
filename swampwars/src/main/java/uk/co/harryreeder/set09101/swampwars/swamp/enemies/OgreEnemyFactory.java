package uk.co.harryreeder.set09101.swampwars.swamp.enemies;

import uk.co.harryreeder.set09101.swampwars.GameState;

import java.util.concurrent.ThreadLocalRandom;

public class OgreEnemyFactory {
	public static OgreEnemy getEnemy(GameState state) {
        int selector = ThreadLocalRandom.current().nextInt(0,3);
        switch (selector) {
            case 0:
                return new Donkey(state);
            case 1:
                return new Parrot(state);
            case 2:
                return new Snake(state);
            default:
                return new OgreEnemy(state);
        }
	}
}
