package uk.co.harryreeder.set09101.swampwars.swamp;

import uk.co.harryreeder.set09101.swampwars.GameState;
import uk.co.harryreeder.set09101.swampwars.swamp.diet.KnightsInShiningArmorDiet;
import uk.co.harryreeder.set09101.swampwars.swamp.diet.OgreDiet;

public class Ogre extends Actor{
	private OgreDiet diet;
	
	public Ogre(String name) {
		super(name);
		this.diet = new KnightsInShiningArmorDiet();
	}

	public Ogre(String name, GameState state) {
		super(name, state);
        this.diet = new KnightsInShiningArmorDiet();
	}

	public OgreDiet getDiet() {
		return this.diet;
	}

	public void setDiet(OgreDiet diet) {
		this.diet = diet;
	}

    public boolean tryEat(int numEnemies) {
        return this.diet.eat(numEnemies);
    }
}
