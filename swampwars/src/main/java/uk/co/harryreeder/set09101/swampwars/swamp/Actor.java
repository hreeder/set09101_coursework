package uk.co.harryreeder.set09101.swampwars.swamp;

import uk.co.harryreeder.set09101.swampwars.GameState;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * Created by harry on 26/11/2015.
 */
public abstract class Actor {
    private Point2D location;
    private String name;
    public GameState state;

    public Actor(String name) {
        this.location = new Point();
        this.name = name;
    }

    public Actor(String name, GameState state) {
        this(name);
        this.registerGameState(state);
    }

    public Point2D getLocation() {
        return this.location;
    }

    public void setLocation(int x, int y) {
        this.location.setLocation(x, y);
        this.updateGameState();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
        this.updateGameState();
    }

    private void registerGameState(GameState state) {
        this.state = state;
    }

    private void updateGameState() {
        this.state.updateGrid();
    }
}
