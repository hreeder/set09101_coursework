package uk.co.harryreeder.set09101.swampwars;

import uk.co.harryreeder.set09101.swampwars.swamp.Actor;
import uk.co.harryreeder.set09101.swampwars.swamp.Ogre;
import uk.co.harryreeder.set09101.swampwars.swamp.enemies.OgreEnemy;
import uk.co.harryreeder.set09101.swampwars.swamp.enemies.OgreEnemyFactory;

import java.awt.geom.Point2D;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by harry on 26/11/2015.
 */
public class GameState {
    // Logging
    private static final Logger logger = Logger.getLogger(App.class.getName());
    public final ArrayList<String> gameLog = new ArrayList<>();

    // Overall state
    private Map<String, Object> state;
    private boolean started = false;

    // The Grid. A Digital Frontier.
    // Our structure is Rows / Cols / Contents (so we can have multiple things in a single location
    private ArrayList<ArrayList<String>> rows;
    private int gameX;
    private int gameY;

    // Actors
    private Ogre ogre;
    private ArrayList<OgreEnemy> enemies;

    public GameState() {}

    /*
        GETTERS AND SETTERS
     */
    public boolean isStarted() {
        return this.started;
    }

    public Map<String, Object> getState() {
        this.state = new HashMap<>();

        this.state.put("started", this.started);

        if (this.started) {
            this.state.put("rows", this.rows);
            this.state.put("diet", this.ogre.getDiet().getDietName());
        }

        ArrayList<String> newestFirstLog = (ArrayList<String>) this.gameLog.clone();
        Collections.reverse(newestFirstLog);

        this.state.put("log", newestFirstLog);

        return this.state;
    }

    public ArrayList<ArrayList<String>> getRows() {
        return this.rows;
    }

    public int getGameX() {
        return this.gameX;
    }

    public int getGameY() {
        return this.gameY;
    }

    public Ogre getOgre() {
        return this.ogre;
    }

    public ArrayList<OgreEnemy> getEnemies() {
        return this.enemies;
    }

    /*
        GAME STATE METHODS
     */
    public void startGame(int gameX, int gameY) {
        this.started = true;
        this.gameX = gameX;
        this.gameY = gameY;
        this.gameLog.clear();

        // It's all ogre now
        // We're going to shortcut adding the state observing the ogre by adding a constructor parameter
        this.ogre = new Ogre("Doogly", this);

        // That'll do, Donkey. That'll do.
        this.enemies = new ArrayList<>();

        // Place the ogre
        int ogreX = ThreadLocalRandom.current().nextInt(0, this.gameX);
        int ogreY = ThreadLocalRandom.current().nextInt(0, this.gameY);

        // The ogre cannot be at 0,0 - if we got that, regenerate
        while (ogreX == 0 && ogreY == 0) {
            ogreX = ThreadLocalRandom.current().nextInt(0, this.gameX);
            ogreY = ThreadLocalRandom.current().nextInt(0, this.gameY);
        }

        ogre.setLocation(ogreX, ogreY);
        this.updateGrid();
    }

    public void updateGrid() {
        // Clear our grid
        this.rows = new ArrayList<>();
        for (int i = 0; i < this.gameX; i++) {
            ArrayList<String> cells = new ArrayList<>();
            for (int j = 0; j < this.gameY; j++) {
                cells.add("");
            }
            this.rows.add(cells);
        }

        // Add our actors to the grid, start with the ogre
        Point2D ogreLoc = ogre.getLocation();
        int ogreX = Double.valueOf(ogreLoc.getX()).intValue();
        int ogreY = Double.valueOf(ogreLoc.getY()).intValue();
        this.rows.get(ogreX).set(ogreY, "<span class='text-primary'><strong>" + ogre.getName() + "</strong></span>");

        for (Actor actor : this.enemies) {
            Point2D actorLoc = actor.getLocation();
            int actorX = Double.valueOf(actorLoc.getX()).intValue();
            int actorY = Double.valueOf(actorLoc.getY()).intValue();
            String name = "<span class='text-danger'>" + actor.getName() + "</span>";

            if (this.rows.get(actorX).get(actorY).equals("")) {
                this.rows.get(actorX).set(actorY, name);
            } else {
                String current = this.rows.get(actorX).get(actorY);
                this.rows.get(actorX).set(actorY, current + ", " + name);
            }
        }
    }

    public void createEnemy() {
        OgreEnemy newEnemy = OgreEnemyFactory.getEnemy(this);

        // Enemies always enter the swamp at 0,0
        newEnemy.setLocation(0,0);

        this.enemies.add(newEnemy);

        // Call our update
        this.updateGrid();
    }

    public void resolvePotentialConflicts() {
        Point2D ogreLoc = this.ogre.getLocation();
        List<OgreEnemy> enemiesAtLoc = this.enemies
                .stream()
                .filter(e -> e.getLocation().equals(ogreLoc))
                .collect(Collectors.toList());

        if (enemiesAtLoc.size() > 0) {
            if (this.ogre.tryEat(enemiesAtLoc.size())) {
                this.gameLog.add(new StringBuilder()
                        .append(this.ogre.getName())
                        .append(" has completed ")
                        .append(String.join(" and ", enemiesAtLoc.stream()
                                .map(e -> e.getName())
                                .collect(Collectors.toList())))
                        .append(".")
                        .toString());
                this.enemies.removeAll(enemiesAtLoc);
            } else {
                this.gameLog.add(new StringBuilder()
                        .append(this.ogre.getName())
                        .append(" died from ")
                        .append(String.join(" and ", enemiesAtLoc.stream()
                                .map(e -> e.getName())
                                .collect(Collectors.toList())))
                        .append(".")
                        .toString());
                this.gameLog.add("Game Over");
                this.started = false;
            }
        }

        this.updateGrid();
    }

    public void reset() {
        this.started = false;
        this.gameX = 0;
        this.gameY = 0;
        this.gameLog.clear();
        this.enemies.clear();
    }
}
