package uk.co.harryreeder.set09101.swampwars.swamp.diet;

/**
 * Created by harry on 27/11/2015.
 */
public class BigMacDiet implements OgreDiet {
    public final String DIET_NAME = "Mince Pies";

    public BigMacDiet() {}

    public boolean eat(int numEnemies) {
        if (numEnemies > 1) {
            return false;
        }
        return true;
    }

    public String getDietName() {
        return this.DIET_NAME;
    }
}
