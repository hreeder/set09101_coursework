package uk.co.harryreeder.set09101.swampwars;

import uk.co.harryreeder.set09101.swampwars.swamp.Actor;

import java.awt.geom.Point2D;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

/**
 * Created by harry on 26/11/2015.
 */
public class MoveCommand implements Runnable {
    private static final Logger logger = Logger.getLogger(MoveCommand.class.getName());
    protected Actor actor;
    protected GameState swamp;

    public MoveCommand(Actor actor, GameState swamp) {
        this.setActor(actor);
        this.setSwamp(swamp);
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public void setSwamp(GameState swamp) {
        this.swamp = swamp;
    }

    public void move() {
        Point2D currentLocation = this.actor.getLocation();
        int currentX = Double.valueOf(currentLocation.getX()).intValue();
        int currentY = Double.valueOf(currentLocation.getY()).intValue();

        // range is x+/-1, y+/-1, x!=0 if y=0 vice versa
        // Don't leave the grid too
        int deltaX = ThreadLocalRandom.current().nextInt(-1, 2);
        int deltaY = ThreadLocalRandom.current().nextInt(-1, 2);

        int newX = currentX + deltaX;
        int newY = currentY + deltaY;

        while ((deltaX == 0 && deltaY == 0) ||
                newX >= this.swamp.getGameX() || newY >= this.swamp.getGameY() ||
                newX < 0 || newY < 0) {
            deltaX = ThreadLocalRandom.current().nextInt(-1, 2);
            deltaY = ThreadLocalRandom.current().nextInt(-1, 2);

            newX = currentX + deltaX;
            newY = currentY + deltaY;
        }

        logger.info("Now moving " + this.actor.getName() + " from " + currentX + "," + currentY + " to " +
                Integer.valueOf(currentX + deltaX).toString() + "," +
                Integer.valueOf(currentY + deltaY).toString());

        this.actor.setLocation(newX, newY);
    }

    public void run() {
        this.move();
    }
}
