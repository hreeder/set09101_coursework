package uk.co.harryreeder.set09101.swampwars.swamp.enemies;

import uk.co.harryreeder.set09101.swampwars.GameState;

/**
 * Created by harry on 27/11/2015.
 */
public class Parrot extends OgreEnemy {
    public Parrot(String name) {
        super(name);
    }

    public Parrot(GameState state) {
        super("Exams", state);
    }

    public Parrot(String name, GameState state) {
        super(name, state);
    }
}
