package uk.co.harryreeder.set09101.swampwars.swamp.diet;

/**
 * Created by harry on 27/11/2015.
 */
public class OgreEnemyDiet implements OgreDiet {
    public final String DIET_NAME = "Coffee";

    public OgreEnemyDiet() {}

    public boolean eat(int numEnemies) {
        if (numEnemies > 2) {
            return false;
        }
        return true;
    }

    public String getDietName() {
        return this.DIET_NAME;
    }
}
