package uk.co.harryreeder.set09101.swampwars.swamp.enemies;

import uk.co.harryreeder.set09101.swampwars.GameState;

/**
 * Created by harry on 27/11/2015.
 */
public class Snake extends OgreEnemy {
    public Snake(String name) {
        super(name);
    }

    public Snake(GameState state) {
        super("MoSP", state);
    }

    public Snake(String name, GameState state) {
        super(name, state);
    }
}
